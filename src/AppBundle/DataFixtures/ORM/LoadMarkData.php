<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/22/16
 * Time: 3:15 AM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Mark;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadMarkData extends AbstractFixture implements OrderedFixtureInterface
{

    private $marksArray = array(
        'American Tourister',
        'Bhppy',
        'Blue Mart',
        'Bree',
        'Bric`s',
        'Burton',
        'CarryOn',
        'Dakine',
        'Decent',
        'Delsey',
        'Eagle Creek',
        'Eastpak',
        'Gabol',
        'Hartmann',
        'Herschel Supply Co.',
        'Jack Wolfskin',
        'Kipling',
        'Oilily',
        'Piquadro',
        'Reisenthel',
        'Rimowa',
        'Roncato',
        'Samsonite',
        'Suit',
        'The North Face',
        'Titan',
        'Travel Bags',
        'Travelite',
        'Tumi',
        'Vaude',
        'Victorinox'
    );

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->marksArray as $title) {
            $mark = new Mark();
            $mark->setTitle($title);

            $manager->persist($mark);
            $manager->flush();

            $this->addReference($title, $mark);
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 0;
    }
}