<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/22/16
 * Time: 3:36 AM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Color;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadColorData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (array('Bronze', 'Black', 'Beige', 'Blue', 'Brown', 'Yellow', 'Gold', 'Gray', 'Green', 'Orange', 'Multicolor', 'Purple', 'Red', 'Rose', 'Taupe', 'White', 'Silver') as $title) {
            $color = new Color();
            $color->setTitle($title);

            $manager->persist($color);
            $manager->flush();

            $this->addReference($title, $color);
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 0;
    }
}