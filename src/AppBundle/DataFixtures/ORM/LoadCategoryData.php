<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/22/16
 * Time: 1:24 AM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{

    private $rootCategories = array(
        array('parent' => 'Suitcases', 'children' => array('Hard Cases', 'Soft Cases', 'Lightweight Suitcases')),
        array('parent' => 'Trolleys', 'children' => array('Laptop Trolleys', 'Grocery Shopping Cards', 'Hand Baggage Trolleys')),
        array('parent' => 'Travel Bags', 'children' => array('Bags With Wheels', 'Sports Bags', 'Weekend Bags', 'Large Bags')),
        array('parent' => 'Laptop Bags', 'children' => array('Backpacks', 'Shoulder Bags')),
        array('parent' => 'School Bags', 'children' => array('School Backpacks', 'School Shoulder Bags')),
        array('parent' => 'Bags', 'children' => array('Mans Bags', 'Kids Bags', 'BriefCases', 'Leather Bags'))
    );

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $root = new Category();
        $root->setTitle(Category::CATEGORY_MENU_ROOT);
        $manager->persist($root);
        foreach ($this->rootCategories as $rootCategory) {
            $parentCategory = new Category();
            $parentCategory->setTitle($rootCategory['parent']);;
            $parentCategory->setParent($root);
            $manager->persist($parentCategory);
            foreach ($rootCategory['children'] as $title) {
                $category = new Category();
                $category->setTitle($title);
                $category->setParent($parentCategory);

                $manager->persist($category);
                $manager->flush();

                $this->addReference($title, $category);
            }
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 0;
    }
}