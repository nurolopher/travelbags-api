<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/20/16
 * Time: 4:26 PM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Bag;
use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBagData extends AbstractFixture implements OrderedFixtureInterface
{

    private $bags = array(
        array('title' => 'Samsonite Accessories Foldable bag indigo',
            'width' => 40,
            'height' => 30,
            'depth' => 24,
            'manufacturers_warranty' => '2 years',
            'serial_number' => '12312313',
            'mark' => 'Samsonite',
            'colors' => array('Yellow', 'Red', 'Black'),
            'material' => 'Leather',
            'categories' => array('Backpacks'),
            'description' => 'This Samsonite folding travel bag is an essential accessory in your luggage. The travel bag is completely folded into a small package and took almost no space in your suitcase or trolley. When this bag unfolded this 55 x 30 x 21 cm and thus meets the IATA standard (allowed as hand luggage on board the aircraft). The bag features a removable and adjustable shoulder strap and double handles. The bag has a handy zip pocket on front.', 'price' => 36.00),
        array('title' => 'Samsonite Cityvibe Laptop Duffle with Wheels 55 exp jet black',
            'width' => 50,
            'height' => 33,
            'depth' => 20,
            'manufacturers_warranty' => '3 years',
            'serial_number' => '32425345',
            'mark' => 'Titan',
            'colors' => array('Red', 'Blue', 'Brown', 'Green', 'Purple', 'Yellow', 'Gray'),
            'material' => 'Cotton',
            'categories' => array('Backpacks', 'Mans Bags'),
            'description' => 'Wanneer je business combineert met casual krijg je de serie Cityvibe van Samsonite. Samsonite antwoord op het nieuwe en groeiende segment van de casual business tas. Deze nieuwe collectie bestaat uit een zachte nylonconstructie met zwarte metalen details en is ideaal op kantoor, in je vrije tijd en om te reizen.

De Cityvibe Laptop Duffle with Wheels 55 exp is een luxe reistas op wielen met een uitneembare 16" laptophoes. Hij is uitbreidbaar met 3 cm / 4.5 liter waardoor de tas vergroot kan worden wanneer nodig. Een combinatieslot met TSA-functie zorgt er voor dat u zorgeloos kunt reizen van en naar de VS.

Kenmerken:

Zachte nylon constructie met zwarte metalen details
Subtiele schaduwprint beschermt tegen krassen
Handbagage volgens IATA-richtlijnen
Combinatiehangslot met TSA-functie
Uitbreidbaar met 3 cm / 4.5 liter
2 Comfortabele wielen
Uitneembare 16" laptophoes
Geschikt voor een iPad
Tablet compartiment
Ergonomische trekstang
Gekruiste inpakriemen
Afneembare schouderriem
Handvatten
Wij zijn officieel Samsonite dealer, u kunt voor de volledige garantie bij ons terecht.', 'price' => 172.00),
        array('title' => 'Dakine EQ Bag 51L Carbon',
            'width' => 100,
            'height' => 40,
            'depth' => 34,
            'price' => 200,
            'manufacturers_warranty' => '5 years',
            'serial_number' => '23234324',
            'mark' => 'Dakine',
            'colors' => array('Red', 'Blue', 'Brown', 'Green', 'Purple', 'Yellow'),
            'material' => 'Leather',
            'categories' => array('Sports Bags', 'Leather Bags', 'Backpacks'),
            'description' => 'The EQ Bag 51L by Dakine is an ideal travel bag for medium holidays. The bag has room for lots of luggage, thanks to the spacious main compartment. Smaller items can be stowed in the zippered pocket on side. The bag comes with a detachable shoulder strap, making the bag can be worn either in hand or over the shoulder. The bag has a U-shaped opening, so that the bag is easily accessible. The bag is made of 600D polyester, this is the bag very sturdy.'),
        array('title' => 'Samsonite Accessories Foldable Travel Bag red',
            'width' => 120,
            'height' => 30,
            'depth' => 37,
            'price' => 210,
            'manufacturers_warranty' => '5 years',
            'serial_number' => '23234322',
            'mark' => 'Samsonite',
            'colors' => array('Red', 'Blue', 'Brown', 'Green', 'Purple', 'Yellow'),
            'material' => 'Polyster',
            'categories' => array('Sports Bags', 'Leather Bags', 'Backpacks'),
            'description' => 'The EQ Bag 51L by Dakine is an ideal travel bag for medium holidays. The bag has room for lots of luggage, thanks to the spacious main compartment. Smaller items can be stowed in the zippered pocket on side. The bag comes with a detachable shoulder strap, making the bag can be worn either in hand or over the shoulder. The bag has a U-shaped opening, so that the bag is easily accessible. The bag is made of 600D polyester, this is the bag very sturdy.'),
        array('title' => 'American Tourister Spring Hill 3-Way Boarding Bag red',
            'width' => 120,
            'height' => 30,
            'depth' => 37,
            'price' => 210,
            'manufacturers_warranty' => '2 years',
            'serial_number' => '23234322',
            'mark' => 'Samsonite',
            'colors' => array('Red', 'Blue', 'Brown', 'Green', 'Purple', 'Yellow', 'Black'),
            'material' => 'Polyster',
            'categories' => array('Sports Bags', 'Leather Bags', 'Backpacks'),
            'description' => 'The EQ Bag 51L by Dakine is an ideal travel bag for medium holidays. The bag has room for lots of luggage, thanks to the spacious main compartment. Smaller items can be stowed in the zippered pocket on side. The bag comes with a detachable shoulder strap, making the bag can be worn either in hand or over the shoulder. The bag has a U-shaped opening, so that the bag is easily accessible. The bag is made of 600D polyester, this is the bag very sturdy.'),
        array('title' => 'Eastpak Stand Weekendtas midnight',
            'width' => 120,
            'height' => 30,
            'depth' => 37,
            'price' => 210,
            'manufacturers_warranty' => '5 years',
            'serial_number' => '23234322',
            'mark' => 'Eastpak',
            'colors' => array('Red', 'Blue', 'Brown', 'Green', 'Purple', 'Orange'),
            'material' => 'Polyster',
            'categories' => array('Sports Bags', 'Leather Bags', 'Backpacks'),
            'description' => 'The EQ Bag 51L by Dakine is an ideal travel bag for medium holidays. The bag has room for lots of luggage, thanks to the spacious main compartment. Smaller items can be stowed in the zippered pocket on side. The bag comes with a detachable shoulder strap, making the bag can be worn either in hand or over the shoulder. The bag has a U-shaped opening, so that the bag is easily accessible. The bag is made of 600D polyester, this is the bag very sturdy.')
    );

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->bags as $bag) {
            $newBag = new Bag();
            $newBag->setTitle($bag['title'])
                ->setDescription($bag['description'])
                ->setPrice($bag['price'])
                ->setWidth($bag['width'])
                ->setHeight($bag['height'])
                ->setDepth($bag['depth'])
                ->setManufacturersWarranty($bag['manufacturers_warranty'])
                ->setSerialNumber($bag['serial_number']);
            $newBag->setMark($this->getReference($bag['mark']));
            $newBag->setMaterial($this->getReference($bag['material']));
            $this->addCategories($newBag, $bag['categories']);
            $this->addColors($newBag, $bag['colors']);
            $manager->persist($newBag);
            $manager->flush();
        }
    }


    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }

    private function addCategories(Bag $newBag, array $categories)
    {
        foreach ($categories as $category) {
            $newBag->addCategory($this->getReference($category));
        }
    }

    private function addColors(Bag $newBag, array $colors)
    {
        foreach ($colors as $color) {
            $newBag->addColor($this->getReference($color));
        }
    }
}