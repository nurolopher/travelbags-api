<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/20/16
 * Time: 11:50 PM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Material;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadMaterialData extends AbstractFixture implements OrderedFixtureInterface
{
    private $materialsArray = array(
        'Aluminium',
        'Nylon',
        'Leather',
        'Cotton',
        'Polyster',
        'Polycarbonate',
        'Polypropylene',
        'Tegris',
        'Tarpaulin',
    );

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->materialsArray as $title) {
            $material = new Material();
            $material->setTitle($title);
            $manager->persist($material);
            $manager->flush();

            $this->addReference($title, $material);
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 0;
    }
}