<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/28/16
 * Time: 12:27 AM
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Custom\Filter',
            'csrf_protection' => false
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');
        $builder
            ->add('colors', EntityType::class, array(
                'class' => 'AppBundle\Entity\Color',
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'id',
                'choice_value' => 'id'
            ))
            ->add('materials', EntityType::class, array(
                'class' => 'AppBundle\Entity\Material',
                'choice_label' => 'id',
                'multiple' => true,
                'expanded' => true
            ))
            ->add('marks', EntityType::class, array(
                'class' => 'AppBundle\Entity\Mark',
                'choice_label' => 'id',
                'multiple' => true,
                'expanded' => true
            ))
            ->add('categories', EntityType::class, array(
                'class' => 'AppBundle\Entity\Category',
                'choice_label' => 'id',
                'multiple' => true,
                'expanded' => false
            ))
            ->add('category', EntityType::class, array(
                'class' => 'AppBundle\Entity\Category',
                'choice_label' => 'slug',
                'choice_value' => 'slug',
                'multiple' => false,
                'expanded' => false
            ));
    }


}