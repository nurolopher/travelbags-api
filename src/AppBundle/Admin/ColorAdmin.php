<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/29/16
 * Time: 11:00 PM
 */

namespace AppBundle\Admin;


use AppBundle\Entity\Color;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ColorAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->add('title', 'text');
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('title');
    }

    /**
     * @param Color $object
     * @return string
     */
    public function toString($object)
    {
        return $object->getTitle();
    }

}