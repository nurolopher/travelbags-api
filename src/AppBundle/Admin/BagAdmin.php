<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/29/16
 * Time: 11:14 PM
 */

namespace AppBundle\Admin;


use AppBundle\Entity\Bag;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class BagAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->with('Main', array('class' => 'col-md-6'))
            ->add('title', 'text')
            ->add('price', 'number')
            ->add('manufacturers_warranty', 'text')
            ->add('serial_number', 'text')
            ->add('description', 'textarea')
            ->end()
            ->with('Dimension', array('class' => 'col-md-6'))
            ->add('width', 'text')
            ->add('height', 'text')
            ->add('depth', 'text')
            ->end()
            ->with('Metadata', array('class' => 'col-md-6'))
            ->add('material', 'sonata_type_model', array(
                'class' => 'AppBundle\Entity\Material',
                'property' => 'title'
            ))
            ->add('colors', 'sonata_type_model', array(
                'class' => 'AppBundle\Entity\Color',
                'property' => 'title',
                'multiple' => true,
                'expanded' => false
            ))
            ->add('categories', 'sonata_type_model', array(
                'class' => 'AppBundle\Entity\Category',
                'property' => 'title',
                'multiple' => true,
                'expanded' => false,
                'by_reference' => false
            ))
            ->end();
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('title')
            ->add('mark.title')
            ->add('material.title')
            ->add('price');
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('material.title');
    }


    /**
     * @param Bag $object
     * @return string
     */
    public function toString($object)
    {
        return $object->getTitle();
    }


}