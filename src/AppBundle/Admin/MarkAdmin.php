<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/29/16
 * Time: 11:04 PM
 */

namespace AppBundle\Admin;


use AppBundle\Entity\Mark;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MarkAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->add('title', 'text');
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('title');
    }

    /**
     * @param Mark $object
     * @return string
     */
    public function toString($object)
    {
        return $object->getTitle();
    }
}