<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/29/16
 * Time: 10:44 PM
 */

namespace AppBundle\Admin;


use AppBundle\Entity\Material;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MaterialAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->add('title', 'text');
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('title');
    }

    /**
     * @param Material $object
     * @return string
     */
    public function toString($object)
    {
        return $object->getTitle();
    }
}