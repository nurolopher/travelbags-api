<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/30/16
 * Time: 1:13 PM
 */

namespace AppBundle\Admin;


use Entity\Category;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CategoryAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form)
    {
        $form->add('title')
            ->add('parent', 'sonata_type_model', array(
                'property' => 'title'
            ));
    }

    protected function configureListFields(ListMapper $list)
    {
        $list->addIdentifier('title');
    }

    /**
     * @param Category $object
     * @return mixed
     */
    public function toString($object)
    {
        return $object->getTitle();
    }


}