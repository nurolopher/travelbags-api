<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/22/16
 * Time: 3:21 AM
 */

namespace AppBundle\Controller\Api;


use FOS\RestBundle\Controller\FOSRestController;

class MarkController extends FOSRestController
{
    public function getMarksAction()
    {
        $marks = $this->getDoctrine()->getRepository('AppBundle:Mark')->findAll();
        $view = $this->view($marks);
        return $this->handleView($view);
    }
}