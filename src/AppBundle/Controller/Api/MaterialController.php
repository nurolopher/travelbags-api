<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/27/16
 * Time: 9:06 PM
 */

namespace AppBundle\Controller\Api;


use FOS\RestBundle\Controller\FOSRestController;

class MaterialController extends FOSRestController
{

    public function getMaterialsAction()
    {
        $materials = $this->getDoctrine()->getRepository('AppBundle:Material')->findAll();
        $view = $this->view($materials);
        return $this->handleView($view);
    }
}