<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/22/16
 * Time: 2:55 AM
 */

namespace AppBundle\Controller\Api;


use AppBundle\Entity\Category;
use FOS\RestBundle\Controller\FOSRestController;

class CategoryController extends FOSRestController
{
    public function getCategoriesAction()
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Category');
        $category = $repository->findOneByTitle(Category::CATEGORY_MENU_ROOT);
        $categories = $repository->getRepoUtils()->childrenHierarchy($category);
        $view = $this->view($categories);
        return $this->handleView($view);
    }

    public function getCategoryAction($id)
    {
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($id);
        return $this->handleView($this->view($category));
    }
}