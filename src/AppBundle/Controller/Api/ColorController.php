<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/22/16
 * Time: 3:34 AM
 */

namespace AppBundle\Controller\Api;


use FOS\RestBundle\Controller\FOSRestController;

class ColorController extends FOSRestController
{
    public function getColorsAction()
    {
        $colors = $this->getDoctrine()->getRepository('AppBundle:Color')->findAll();
        $view = $this->view($colors);
        return $this->handleView($view);
    }

    public function getColorAction($id)
    {
        $color = $this->getDoctrine()->getRepository('AppBundle:Color')->find($id);
        $view = $this->view($color);
        return $this->handleView($view);
    }
}