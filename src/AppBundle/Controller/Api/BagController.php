<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Bag;
use AppBundle\Entity\Custom\Filter;
use AppBundle\Form\FilterType;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class BagController extends FOSRestController
{
    public function getBagsAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Bag');

        $filter = new Filter();
        $form = $this->createForm(FilterType::class, $filter);
        $form->handleRequest($request);
        $bags = $form->isValid() ? $repository->findByFilter($filter) : $repository->findAll();
        $view = $this->view($bags);
        return $this->handleView($view);
    }

    /**
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getBagAction($slug)
    {
        /** @var Bag $bag */
        $bag = $this->getDoctrine()->getRepository('AppBundle:Bag')->findOneBySlug($slug);
        return $this->handleView($this->view($bag));
    }
}
