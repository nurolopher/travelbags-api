<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 6/28/16
 * Time: 12:15 AM
 */

namespace AppBundle\Entity\Custom;


use AppBundle\Entity\Color;
use AppBundle\Entity\Mark;
use AppBundle\Entity\Material;
use Doctrine\Common\Collections\ArrayCollection;
use Entity\Category;

class Filter
{
    /** @var  ArrayCollection | Mark[] */
    private $marks;

    /** @var  ArrayCollection | Color[] */
    private $colors;

    /** @var  ArrayCollection | Material[] */
    private $materials;

    /** @var ArrayCollection | Category[] */
    private $categories;

    /** @var Category */
    private $category;

    public function __construct()
    {
        $this->marks = new ArrayCollection();
        $this->materials = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->colors = new ArrayCollection();
    }


    /**
     * @return \AppBundle\Entity\Mark[]|ArrayCollection
     */
    public function getMarks()
    {
        return $this->marks;
    }

    /**
     * Add mark
     *
     * @param Mark $mark
     *
     * @return Filter
     */
    public function addMark(Mark $mark)
    {
        $this->marks[] = $mark;

        return $this;
    }

    /**
     * Remove mark
     *
     * @param Mark $mark
     */
    public function removeMark(Mark $mark)
    {
        $this->marks->removeElement($mark);
    }

    /**
     * @return \AppBundle\Entity\Color[]|ArrayCollection
     */
    public function getColors()
    {
        return $this->colors;
    }

    /**
     * @param Color $color
     * @return $this
     */
    public function addColor(Color $color)
    {
        $this->colors[] = $color;
        return $this;
    }

    /**
     * @param Color $color
     */
    public function removeColor(Color $color)
    {
        $this->colors->removeElement($color);
    }

    /**
     * @return \AppBundle\Entity\Material[]|ArrayCollection
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    /**
     * @param Material $material
     * @return $this
     */
    public function addMaterial(Material $material)
    {
        $this->materials[] = $material;
        return $this;
    }

    /**
     * @param Material $material
     */
    public function removeMaterial(Material $material)
    {
        $this->materials->removeElement($material);
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;
        return $this;
    }

    /**
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * @return ArrayCollection|\Entity\Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param $string
     * @return array
     */
    public function getIndexesOf($string)
    {
        return $this->{'get' . ucfirst($string)}()->map(function ($entity) {
            return $entity->getId();
        })->toArray();
    }

    /**
     * @param Category $category
     * @return Filter
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }
}

