<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Color
 *
 * @ORM\Table(name="app_color")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ColorRepository")
 * @ExclusionPolicy("none")
 */
class Color
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     */
    private $title;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Bag", mappedBy="colors")
     * @Exclude()
     */
    private $bags;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Color
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bag
     *
     * @param \AppBundle\Entity\Bag $bag
     *
     * @return Color
     */
    public function addBag(\AppBundle\Entity\Bag $bag)
    {
        $this->bags[] = $bag;

        return $this;
    }

    /**
     * Remove bag
     *
     * @param \AppBundle\Entity\Bag $bag
     */
    public function removeBag(\AppBundle\Entity\Bag $bag)
    {
        $this->bags->removeElement($bag);
    }

    /**
     * Get bags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBags()
    {
        return $this->bags;
    }
}
